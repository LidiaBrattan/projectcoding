import java.util.Scanner;

public class Application {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);

        System.out.println("Va rog alegeti o optiune: ");
        System.out.println("1. Calculati diametrul cercului");
        System.out.println("2. Calculati BMI");
        System.out.println("3. Calculati ecuatia patratica");
        System.out.println("4. Boltz");

        int optUser = keyboard.nextInt();
        switch (optUser) {
            case 1:
                diamPerim(keyboard); //declari aici keyboard pt ca e doar un scanner in main. Daca faceai un scanner la fiecare metoda nu trebuia sa mai declari
                break;
            case 2:
                bodyMassIndex(keyboard);
                break;
            case 3:
                quadraticEquation(keyboard);
                break;
            case 4:
                fizzBuzz(keyboard);
                break;




        }


    }


    public static void diamPerim(Scanner keyboard) {
        System.out.println("Dati diamerul cercului pentru a calcula perimetrul");
        float diam = keyboard.nextFloat();
        double pi = Math.PI;
        double perim = pi * diam;
        System.out.println("Perimetrul cercului este " + perim);
    }

    public static void bodyMassIndex(Scanner keyboardForMaxIndex) { //atunci cand va fi apelata functia ne asteptam sa primim valoare
        //declarare variabile
        float weightKg = 0;
        float heightM = 163;
        int heightCm = 0;
        // BMI = wight [kg] / height [m]^2

        //informare utilizator si citire de la tastatura
        System.out.println("Introduceti greutatea in kg: ");
        weightKg = keyboardForMaxIndex.nextFloat();
        System.out.println("Introduceti inaltimea in cm: ");
        heightCm = keyboardForMaxIndex.nextInt();

        heightM = heightCm / 100f;
        double heightInMeatherSquared = Math.pow(heightM, 2);
        double bMI = weightKg / heightInMeatherSquared;
        System.out.println("Indexul de masa corporala este: " + bMI);

        if (bMI > 18.5 && bMI < 24.9) {
            System.out.println("Userul este normal ponderat!");
        } else {
            System.out.println("Userul nu este normal ponderat!");
        }

    }

    public static void quadraticEquation(Scanner keyboard) {
        System.out.println("Va rog sa introduceti parametrii a,b si c: ");
        int a;
        int b;
        int c;
        a = keyboard.nextInt();
        b = keyboard.nextInt();
        c = keyboard.nextInt();
        int delta = b * b - 4 * a * c;
        if (delta < 0) {
            System.out.println("Ecuatia nu are solutii");
        } else {
            double x1;
            double x2;
            //(x1 = -b + radical din delta) /2a
            x1 = (-b + Math.sqrt(delta)) / (2 * a);
            x2 = (-b - Math.sqrt(delta)) / (2 * a);
            System.out.println("Solutiile sunt: x1 = " + x1 + ";" + " si x2 = " + x2 + ".");

        }

    }

    public static void fizzBuzz(Scanner keyboard) {
        System.out.println("Introduceti un numar: ");
        int positiveNr;
        positiveNr = keyboard.nextInt();
        for (int n = 1; n <= positiveNr; n++) {
            if ((n % 3 ==0) && (n % 7 == 0)){
                System.out.println("Fizz Buzz");
            }else if (n % 3 == 0) {
                System.out.println("Fizz");
            } else if  (n%7==0){
                System.out.println("Buzz");
            } else {
                System.out.println(n);
            }


        }



    }


    public static void isPrime(){

    }


}
