import java.util.Scanner;

public class ExercitiuDoi {

    static boolean isPerfectSquare(double x){
        double sq = Math.sqrt(x);
        return ((sq - Math.floor(sq)) ==0);

    }

    public static void main(String[] args) {
        System.out.println("Introduce-ti un nr: ");
        Scanner keyboard = new Scanner(System.in);
        double n = keyboard.nextDouble();
        keyboard.close();

        if(isPerfectSquare(n))
            System.out.println(Math.round(n) + " este un numar patrat perfect!");
        else
            System.out.println(Math.round(n) + " nu este un numar patrat perfect!");
    }
}
