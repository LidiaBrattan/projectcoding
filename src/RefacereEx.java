import java.text.NumberFormat;
import java.util.Scanner;

public class RefacereEx {
    public static void main(String[] args) {

        final byte monthInYear = 12;
        final byte percent = 100;
        Scanner scan = new Scanner(System.in);
        /** vrei sa primesti principal : 100_000 (minim 1k si max 1 mil)
         * anual interest rate : 3.92%   (3.92/100)/12 --> pt monthly interest
         * period (Years) : 30
         * program that calculates your monthly payments and displays it as a curency
         * mortgage = principal * monthly interest rate
         */
        int principal = 0;

        while (true) {
            System.out.println("Princial: ");
            principal = scan.nextInt();
            if (principal> 1000 && principal <= 1000000)
                break;
            System.out.println("Enter a value between 1000 and 1000000");
        }
        float annualInterestRate;
        float monthlyInterestRate;

        while (true) {
            System.out.println("Annual interest rate ");
            annualInterestRate = scan.nextFloat();
            if (annualInterestRate>=1 && annualInterestRate<=30) {
                monthlyInterestRate = annualInterestRate / percent / monthInYear;
                break;
            }
            System.out.println("Enter a value between 1 and 30");
        }
        System.out.println("Period (years) ");
        int numberOfPayments;
        while (true) {
            byte years = scan.nextByte();
            if (years>=1 && years<=30) {
                numberOfPayments = years * monthInYear;
                break;
            }
            System.out.println("Enter a value between 1 and 30");
        }
        double mortgage = principal
                * (monthlyInterestRate * Math.pow(1+ monthlyInterestRate, numberOfPayments))
                / (Math.pow(1 + monthlyInterestRate, numberOfPayments) -1);
        String mortgageFormatted = NumberFormat.getCurrencyInstance().format(mortgage);

        System.out.println("Mortgage: " + mortgageFormatted);

    }
}
