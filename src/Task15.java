import java.util.Scanner;

public class Task15 {

    /** Aplicatia asta returneaza fiecare cuvint repetat
     * ex: Hei, eu sunt Lidia! --> Hei Hei eu eu sunt sunt Lidia Lidia
     * optiuni:
     * 1. Citim textul de la tastatura
     * 2. Cream un array de cuvinte din textul citit
     * 3. Din acel array il parcurgem de cate 2 ori intr-un nou array
     * 4. Afisam ultimul array creat
     */

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        //variabila unde vom pune string-ul nebalbait
        String normal ="";
        String[] arrayNormal;
        //declarat array-ul pt string balbait
        String balbait = " ";
        System.out.println("Introduceti o fraza.");

        //am citit de la tastatura stringul si l-am pus in stringurl normal
        normal = keyboard.nextLine();
        arrayNormal = normal.split(" ");

        for ( int i = 0; i <= arrayNormal.length -1; i++){
            String cuvant = arrayNormal[i];
            balbait = balbait + " " + cuvant + " " + cuvant;
        }


        System.out.println(balbait);












        //    String str = "Ana are mere";
//        str.split(" ");
//    // {"Ana", "are", "mere"}
//        str.split("m");
//    // {"Ana are ", "ere"}
//    str = "Str Primăverii, bloc C4, ap 45";
//        str.split(",");
//    // {"Str Primăverii", "bloc C4", "ap 45"}


    }
}
