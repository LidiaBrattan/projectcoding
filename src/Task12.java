import java.util.Scanner;

public class Task12 {
    public static void main(String[] args) {
        /**
         * nr de spatii =
         * nr total caractere =
         * ? % reprezinta nr spatii din total
         * spatii..........total
         * x..........100
         * percentage of occurencies = (nr spatii x 100)/total
         * x=3*100/21
         */

        Scanner scanner = new Scanner(System.in);
//        System.out.println("Introduce-ti caractere");
//
//        String caractere = scanner.nextLine();
//        int spatii = 0;
//        int totalCaractere = caractere.length();
//        float calculProcent;
//
//        for( int i = 0; i < caractere.length(); i++){
//            if (caractere.charAt(i) == ' '){
//                spatii = spatii +1;
//            }
//        }
//        calculProcent = (spatii * 100) / totalCaractere;
//        System.out.println(calculProcent);
//        numarDivizibilCu3(scanner);
//        careNumarSeDivide(scanner);
        esteOIntrebare(scanner);

    }

    public static void numarDivizibilCu3(Scanner scanner) {
        System.out.println("Introduceti un numar: ");
        int numar = scanner.nextInt();

        if ((numar % 5 == 0) || (numar % 3 == 0) || (numar % 3 == 0)) {
            System.out.println("Se divide cu 5 ,7 sau cu 3!");
        } else {
            System.out.println("Nu se divide cu 5, 7 sau cu 3!");
        }
    }

    public static void careNumarSeDivide(Scanner scanner) {

        System.out.println("Introduceti un nr ce trebuie divizat: ");
        int numarCeTrebuieDivizat = scanner.nextInt();

        System.out.println("Introduceti divizorul: ");
        int divizorul = scanner.nextInt();

        if (numarCeTrebuieDivizat % divizorul == 0) {
            System.out.println("Numarul " + numarCeTrebuieDivizat + " se divide cu " + divizorul + ".");

        } else {
            System.out.println("Numarul " + numarCeTrebuieDivizat + " nu se divide cu " + divizorul + ".");
        }
    }

    public static void esteOIntrebare(Scanner scanner) {
        System.out.println("Scrieti un text: ");
        String text = scanner.nextLine();
        int nrVocale = 0;
        String vocale = "aeiou";
        boolean intrebare = false;

        for (int i = 0; i < text.length(); i++) {
            if (vocale.contains(text.charAt(i) + "")){
                nrVocale ++;
                intrebare = true;
            }
        }
        if (true == intrebare) {
            System.out.println("Contine " + nrVocale + " vocale.");
        } else {
            System.out.println("Nu contine vocale.");
        }
    }
}
