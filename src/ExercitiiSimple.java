import java.util.Scanner;

public class ExercitiiSimple {
    public static void main(String[] args) {

        Scanner keyboard = new Scanner(System.in);
        nr(keyboard);

    }

    public static void nrPare(Scanner keyboard) {
        /**
         * de-a un nrPar par
         * daca nu e par : Hei asta nu e nrPar par
         * daca este par: cere si unui impar
         * Care sunt toate numerele dintre cele 2 numere puse de utilizator
         *
         * Tema 1: unde pui sout si for (nu sunt bune)
         * Tema 2: ia in considerare si daca primul nrPar e mai mare, nu doar al 2 lea
         */
        System.out.println("Introduceti numele: ");
        String nume = keyboard.nextLine();
        System.out.println("Servus, " + nume + " ! ");
        System.out.println("Da-mi un nrPar care-i par: ");
        int nrPar = keyboard.nextInt();
        int nrImpar = 0;

        if (nrPar % 2 != 0) {
            System.out.println("Hei, asta nu este un nr par.");
        } else {
            System.out.println("Introduceti si un numar impar.");
            nrImpar = keyboard.nextInt();
        }
        if (nrPar % 2 == 0) {
            System.out.println("Numerele cuprinse intre cele introduse de dvs sunt: ");
            for (int i = nrPar; i <= nrImpar; i++) {
                System.out.println(i);
            }

        }
    }
    //scrie cate nr vrei sa verifici
    //si cu cine vrei sa divizi acel numar
    public static void nr(Scanner keyboard) {
        System.out.println(" Introduceti numere la tastatura: ");
        int n = keyboard.nextInt();
        int n2 = 0;
        System.out.println("Introduceti nr cu care sa se divida: ");
        int ndiv = keyboard.nextInt();
        System.out.println("Se divide cu: ");

        for (int i = 1; i <= n; i++) {
            n2 = keyboard.nextInt();
            if (n2 % ndiv == 0) {
                System.out.println("Numarul se divide cu " + ndiv + ".");
            } else {
                System.out.println("Nu se divide cu " + ndiv + ".");
            }

        }
    }


}




