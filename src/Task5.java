import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        /**Write an application that takes a positive number from the user (type int) and prints all
         *prime numbers greater than 1 and less than the given number.
         *
         * luam fiecare nr intre 1 si nr citit
         * verificam pentru fiecare in parte daca e prim
         */
        Scanner keyboard = new Scanner(System.in);

        afiseazaNrPrimePanaLa(keyboard);
    }

    public static boolean isPrime(int nrPrim) {
        int counter = 0;
        for (int i = 2; i < nrPrim; i++) {
            if (nrPrim % i == 0) {
                counter = counter + 1;
            }
        }
        if (counter == 0) {
            return true;
        } else {
            return false;
        }
    }

    public static void primeScurt(Scanner keyboard) {
        int numarTastatura;
        numarTastatura = keyboard.nextInt();
        boolean estePrim;
        estePrim = isPrime(numarTastatura);
        if (estePrim == true) {
            System.out.println("Este prim");
        } else {
            System.out.println("Nu este prim");
        }
    }

    public static void afiseazaNrPrimePanaLa (Scanner keyboard){
        System.out.println("Introduceti un numar: ");
        int nr = keyboard.nextInt();
        System.out.println("Numerele prime sunt: ");
        for (int a = 1; a <= nr; a++){
            boolean aEstePrim = isPrime(a);

            /**pt ca vrem sa vedem daca numerele a vor fi prime, nr este nr de la tastatura iar
             * iar a vor fi nuemrele pana la nr. a vor fi cele afisate si vrem sa arate doar pe cele prime
             */

            if (aEstePrim == true){

                /**se mai poate face si:
                 * if (isPrime(a) == true) {....
                 * atunci, fara boolean.
                 */

                System.out.println(a);
            }
        }
    }

    public static void primeLung(Scanner keyboard) {
        System.out.println("Introduceti un numar intre 1 si 100_000: ");
        int numar = keyboard.nextInt();
        int counter = 0;
        if ((numar >= 1) && (numar <= 100_000)) {
            //
            System.out.println("Ati introdus un numar corect");
            for (int i = 2; i < numar; i++) {
                if (numar % i == 0) {
                    counter = counter + 1;
                    System.out.println(i);
                }
            }// punem sout in in primul if, al 2 lea este in for si atunci nu e bine
            if (counter == 0) {
                System.out.println("Numarul introdus este prim.");
            } else {
                System.out.println("Numarul introdus nu este prim.");
            }

            System.out.println("Numarul dat are: " + counter + " divizori"); // ca sa nu fie in for altfel s-ar fi iterat de multe ori
        } else {
            System.out.println("Nu ati introdus un numar corect");
        }
    }
}
