import javax.sound.midi.Soundbank;
import java.util.Scanner;

public class CalculatorEx8 {
    /**Userul introduce nr 10
     * 10
     * +
     * 20
     * Rezultatul este =
     */
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);

        System.out.println("Introduceti primul numar: ");
        float a = keyboard.nextFloat();
        System.out.println("Va rugam selectati una dintre operatii: +, - , *, /");
        keyboard.nextLine(); //ca sa proceseze enterul
        char semn = keyboard.next().charAt(0); //la char pui caracterul de la pozitia 0, adica indexul
        System.out.println("Introduceti al doilea numar: ");
        float b = keyboard.nextFloat();
        float rezultat = 0;

        switch (semn) {
            case '+': // in cazul in care pica +.....
                rezultat = a + b;
                break;
            case '-':
                rezultat = a - b;
                break;

            case '*':
                rezultat = a * b;
                break;
            case '/':
                rezultat = a / b;
                break;
            default:
                System.out.println("Operatia nu este valida");
        }
        System.out.println("Rezultatul este: " + rezultat);

    }
}
