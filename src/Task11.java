import java.util.Scanner;

public class Task11 {
    public static void main(String[] args) {
    execute();
    }

    /**
     * Write an application that will read texts (variables of the String type) until the user gives
     * the text "Enough!" and then writes the longest of the given texts (not including the text
     * "Enough!"). If the user does not provide any text, write "No text provided"
     */

    public static void execute() {
        Scanner keyboard = new Scanner(System.in);

        String line = keyboard.nextLine();
        String longestLine = "";
        int longest = 0;

        if (line.isEmpty()) {
            System.out.println("No text provided");
            return; //iese din functie, se termina programul
        }
        longest = line.length();
        longestLine = line;
        //loop: for si while, in cazul acesta ne trebuie un while
        //pentru obiecte comparam cu metoda .equals()
        while (!line.equals("Enough!")) {
            if (line.length() > longest) {
                longest = line.length();
                longestLine = line;
            }
            line = keyboard.nextLine();
        }
        System.out.println(longestLine);

    }
}
